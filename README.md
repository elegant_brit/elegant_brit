Elegant-Brit
============

Gnome Theme
Licenced GPL2 October 2014


"This theme is a mod of Elegant One, by Dzakusek (which was also a mod of Black-White, by Lyrae). When I saw Glossy Fall, created by Crimesaucer, I fell in love with the colors and decided I just had to apply them to the Elegant concept. Red and blue would be my master inspirations.
After some more personal changes, I came out with what I decided to call Elegant Brit. I hope you enjoy it." 
- fmrbpensador 2008 

http://gnome-look.org/content/show.php/Elegant+Brit?content=74553

I can't remember where I picked up Elegant Brit as a theme but it was a while ago. When Gnome 3 was launched I found that the origional theme was not being updated.

grvrulz on DeviantArt had updated it for 3.0 and 3.2 but then life got in the way. I picked up some fixes aftermarketgirl had done and have packaged it for 3.6.

I'm not very good at this, I've no idea how style sheets cascade and there are a lot of wierd bugs that have cropped up. I can't even find a theming community to ask questions in and I don't understand the gnome docs.

Despite these minor handicapps I try to keep the theme up to date with the latest Gnome, Please help.
